#!/usr/bin/env python3
"""Shell interface for pipeline kcidb data."""
import argparse

from cki_lib import misc
from cki_lib.kcidb.file import KCIDBFile


def kcidb_set(kcidb_file, args):
    """Set key=value in toplevel_object."""
    getattr(kcidb_file, args.toplevel_object)[args.key] = args.value
    kcidb_file.save()


def kcidb_get(kcidb_file, args):
    """Print value for key from toplevel_object."""
    print(getattr(kcidb_file, args.toplevel_object)[args.key])


def create_checkout(kcidb_file, args):
    """Create checkout."""
    kcidb_file.checkout = {
        'id': args.id,
        'origin': 'redhat',
    }
    kcidb_file.save()


def create_build(kcidb_file, args):
    """Create build."""
    kcidb_file.build = {
        'id': args.id,
        'origin': 'redhat',
        'checkout_id': args.checkout_id,
    }
    kcidb_file.save()


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='Edit a KCIDB file.'
    )

    parser.add_argument(
        'kcidb_file',
        type=str,
        help='A path to the KCIDB file to read/write.',
        default=None,
    )

    subparsers = parser.add_subparsers()

    # get and set
    parser_get = subparsers.add_parser('get')
    parser_get.set_defaults(func=kcidb_get)

    def add_common_get_set_args(parser):
        parser.add_argument(
            'toplevel_object',
            choices=['checkout', 'build'],
            help='Which toplevel KCIDB object to operate on.',
        )
        parser.add_argument(
            'key',
            type=str,
        )

    add_common_get_set_args(parser_get)

    for name, value_type in (
        ('', str),
        ('-bool', misc.strtobool),
        ('-int', int),
    ):
        subparser = subparsers.add_parser(f'set{name}')
        subparser.set_defaults(func=kcidb_set)
        add_common_get_set_args(subparser)
        subparser.add_argument(
            'value',
            type=value_type,
        )

    # create
    parser_create = subparsers.add_parser('create')
    create_subparsers = parser_create.add_subparsers()

    # create checkout
    parser_create_checkout = create_subparsers.add_parser('checkout')
    parser_create_checkout.set_defaults(func=create_checkout)
    parser_create_checkout.add_argument(
        'id',
        type=str,
    )

    # create build
    parser_create_build = create_subparsers.add_parser('build')
    parser_create_build.set_defaults(func=create_build)
    parser_create_build.add_argument(
        'id',
        type=str,
    )
    parser_create_build.add_argument(
        'checkout_id',
        type=str,
    )

    return parser.parse_args()


def main():
    """Open file and run appropriate function."""
    args = parse_args()

    kcidb_file = KCIDBFile(args.kcidb_file)

    args.func(kcidb_file, args)


if __name__ == '__main__':
    main()
