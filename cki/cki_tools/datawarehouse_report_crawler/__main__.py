"""Datawarehouse report crawler."""
import argparse
import datetime
import os
import sys
import tempfile

from cki_lib.logger import get_logger

from . import helpers
from .classes import LatestMailArchiveCrawler
from .classes import MMessageBase

LOGGER = get_logger('cki.cki_tools.datawarehouse_report_crawler')


def filter_messages(messages_list, datawarehouse_url):
    """Remove unwanted messages."""
    messages = []

    for message in messages_list:
        # Filter out messages without checkout_iid
        if not message.checkout_iid:
            if not message.pipelineid:
                continue

            # Try to match it to a checkout anyway
            try:
                message.checkout_iid = helpers.get_checkout_iid(
                    datawarehouse_url, message.pipelineid)
            except Exception:  # pylint: disable=broad-except
                continue

        # Not a result email
        if not (
                'SKIPPED: ' in message.msg['Subject'] or
                'PASS: ' in message.msg['Subject'] or
                'PANICKED: ' in message.msg['Subject'] or
                'FAIL: ' in message.msg['Subject']):
            continue

        # Filter out messages older than 7 days.
        message_datetime = datetime.datetime.strptime(
            message.msg['X-List-Received-Date'],
            '%a, %d %b %Y %H:%M:%S %z'
        ).replace(tzinfo=None)
        last_week = datetime.datetime.now() - datetime.timedelta(days=7)
        if message_datetime < last_week:
            continue

        # Fallback. Add to the list.
        messages.append(message)

    return messages


def process_message(datawarehouse_url, datawarehouse_token, message, action):
    """Process the email and report to DW if necessary."""
    try:
        helpers.get_checkout_report(datawarehouse_url, message)
        return
    except Exception:  # pylint: disable=broad-except
        LOGGER.info("Report does not exist. checkout_iid=%s message_id=%s",
                    message.checkout_iid, message.msg.get('Message-ID'))

    if action == 'submit':
        helpers.submit_report(datawarehouse_url, datawarehouse_token, message)
    else:
        print(f'Report for {message.checkout_iid} should be submitted')


def main(args):
    """Run, Forest, run!."""
    parser = argparse.ArgumentParser(description='Submit sent reports to the Data Warehouse')
    parser.add_argument('--datawarehouse-url', default=os.environ.get('DATAWAREHOUSE_URL'),
                        help='URL of the DataWarehouse instance for submitting reports')
    parser.add_argument('--datawarehouse-token', default=os.environ.get('DATAWAREHOUSE_TOKEN'),
                        help='Name of the env variable with the DataWarehouse token')
    parser.add_argument('--mailing-list-url', default=os.environ.get('MAILING_LIST_URL'),
                        help='URL of the Mailman mailing list archive')
    parser.add_argument('--action', default=os.environ.get('ACTION', 'report'),
                        choices=('report', 'submit'),
                        help='Whether to only report or submit new reports')
    parsed_args = parser.parse_args(args)

    with tempfile.TemporaryDirectory() as tempdir:
        cache_path = f'{tempdir}/cache'
        crawler = LatestMailArchiveCrawler(parsed_args.mailing_list_url,
                                           None, f'{tempdir}/cookies')
        crawler.update_listarchive(cache_path)

        messages = MMessageBase.listarchives2msgs(cache_path)
        messages = filter_messages(messages, parsed_args.datawarehouse_url)

        for message in messages:
            process_message(parsed_args.datawarehouse_url,
                            os.environ[parsed_args.datawarehouse_token],
                            message, parsed_args.action)


if __name__ == '__main__':
    main(sys.argv[1:])
