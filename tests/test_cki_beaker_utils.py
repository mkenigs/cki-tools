"""Test CKI Beaker Tools Utils."""
import itertools
import unittest

from cki.beaker_tools.utils import decode_test_result

POSSIBLE_STATUSES = [
    'Aborted', 'Completed', 'Cancelled', 'New', 'Queued', 'Waiting',
    'Running', 'Processed', 'Scheduled'
]
POSSIBLE_RESULTS = ['Pass', 'Skip', 'Warn', 'Fail', 'Panic', 'New']


class TestUtils(unittest.TestCase):
    """Test Beaker utils."""

    def test_decode_test_result(self):
        """Test all combinations for decode_test_result."""
        test_cases = (
            # status, result, skipped, expected
            # All green.
            ('Completed', 'Pass', False, 'PASS'),
            # Task Fail.
            ('Completed', 'Fail', False, 'FAIL'),
            # Task Skipped.
            ('Completed', 'Skip', False, 'SKIP'),

            # Task Panic.
            ('Aborted', 'Panic', False, 'FAIL'),
            # Task Aborted due to EWD while running.
            ('Aborted', 'Warn', False, 'FAIL'),
            # Task Aborted due to EWD didn't run.
            ('Aborted', 'Warn', True, 'SKIP'),

            # Anything skipped is skip.
            (None, None, True, 'SKIP'),

            # Anything not Completed/Aborted is SKIP.
            ('Cancelled', None, False, 'SKIP'),
            ('New', None, False, 'SKIP'),
            ('Queued', None, False, 'SKIP'),
            ('Waiting', None, False, 'SKIP'),
            ('Running', None, False, 'SKIP'),
            ('Scheduled', None, False, 'SKIP'),
        )

        for status, result, skipped, expected in test_cases:
            selected_status = [status, ] if status else POSSIBLE_STATUSES
            selected_result = [result, ] if result else POSSIBLE_RESULTS

            for loop_status, loop_result in \
                    itertools.product(selected_status, selected_result):
                self.assertEqual(
                    expected,
                    decode_test_result(loop_status, loop_result, skipped),
                    (loop_status, loop_result, skipped, expected)
                )
