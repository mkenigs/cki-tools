"""Test edit.py"""
import contextlib
import io
import json
from pathlib import Path
import tempfile
import unittest
from unittest import mock

from cki_lib.kcidb.file import KCIDBFile

from cki.kcidb.edit import main


class TestKCIDBEdit(unittest.TestCase):
    """Test edit.py."""

    # Copied from cki_lib
    checkout1 = {
        'id': 'redhat:checkout1',
        'origin': 'redhat'
    }

    # Copied from cki_lib
    build1 = {
        'id': 'redhat:build1',
        'checkout_id': 'redhat:checkout1',
        'origin': 'redhat'
    }

    # Copied from cki_lib
    valid_content = {
        'version': {'major': 4},
        'tests': [],
    }

    # Copied from cki_lib
    @classmethod
    @contextlib.contextmanager
    def dump_kcidb_json(cls, kcidb_data):
        tmpdir = tempfile.TemporaryDirectory()
        path = Path(tmpdir.name, 'kcidb_data.json')
        path.write_text(json.dumps(kcidb_data))
        try:
            yield path
        finally:
            tmpdir.cleanup()

    # Copied from cki_lib
    @classmethod
    @contextlib.contextmanager
    def create_kcidb_file(cls, kcidb_data):
        with cls.dump_kcidb_json(kcidb_data) as path:
            kcidb_file = KCIDBFile(str(path))
            try:
                yield (path, kcidb_file)
            finally:
                pass

    def test_set(self):
        """Test set."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        for command, key, value in (
            ('set', 'id', 'redhat:build2'),
            ('set-bool', 'valid', True),
            ('set-int', 'duration', 0),
        ):
            with self.subTest(command=command):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    with mock.patch(
                        'sys.argv',
                        ['edit.py', str(kcidb_file_path), command, 'build', key, str(value)]
                    ):
                        main()
                    kcidb_file2 = KCIDBFile(str(kcidb_file_path))
                    self.assertEqual(value, kcidb_file2.build[key])

    def test_set_raises_type(self):
        """Test set-bool and set-int raise if the value passed is not the correct type."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        for command, key, value in (
            ('set-bool', 'valid', 'Tru'),
            ('set-int', 'duration', '0..'),
        ):
            with self.subTest(command=command):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    with mock.patch(
                        'sys.argv',
                        ['edit.py', str(kcidb_file_path), command, 'build', 'valid', value]
                    ):
                        with self.assertRaises(SystemExit):
                            main()

    def test_set_raises_partial(self):
        """Test set raises if a valid attribute is set but the file is still not valid."""
        for attr in ('build', 'checkout'):
            with self.subTest(attr=attr):
                with self.create_kcidb_file(self.valid_content) as (kcidb_file_path, kcidb_file):
                    id = f'redhat:{attr}2'
                    with mock.patch('sys.argv',
                                    ['edit.py', str(kcidb_file_path), 'set', attr, 'id', id]):
                        with self.assertRaises(Exception):
                            main()

    def test_get(self):
        """Test get."""
        for attr, kcidb_data in (
            ('build', {**self.valid_content, 'builds': [self.build1]}),
            ('checkout', {**self.valid_content, 'checkouts': [self.checkout1]}),
        ):
            with self.subTest(attr=attr):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    with mock.patch('sys.argv',
                                    ['edit.py', str(kcidb_file_path), 'get', attr, 'id']):
                        with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                            main()
                            self.assertEqual(mock_stdout.getvalue(),
                                             f'{kcidb_data[f"{attr}s"][0]["id"]}\n')

    def test_create(self):
        for attr, args in (
            ('checkout', [self.checkout1['id']]),
            ('build', [self.build1['id'], self.build1['checkout_id']]),
        ):
            with tempfile.TemporaryDirectory() as tmpdir:
                kcidb_file_path = Path(tmpdir, 'kcidb_data.json')
                with mock.patch('sys.argv',
                                ['edit.py', str(kcidb_file_path), 'create', attr] + args):
                    main()
                    self.assertEqual(
                        getattr(KCIDBFile(str(kcidb_file_path)), attr),
                        getattr(self, f'{attr}1'),
                    )

    def test_create_raises(self):
        for attr, args in (
            ('checkout', ['INVALID_ID']),
            ('build', ['INVALID_ID', 'INVALID_CHECKOUT_ID']),
        ):
            with tempfile.TemporaryDirectory() as tmpdir:
                kcidb_file_path = Path(tmpdir, 'kcidb_data.json')
                with mock.patch('sys.argv',
                                ['edit.py', str(kcidb_file_path), 'create', attr] + args):
                    with self.assertRaises(Exception):
                        main()
